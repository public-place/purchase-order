﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PurchaseOrder.Data.Migrations
{
    public partial class Extend_PurchaseOrder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PurchaseOrder_AspNetUsers_UserId",
                table: "PurchaseOrder");

            migrationBuilder.DropIndex(
                name: "IX_PurchaseOrder_UserId",
                table: "PurchaseOrder");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "PurchaseOrder");

            migrationBuilder.AddColumn<string>(
                name: "ApplicationUserId",
                table: "PurchaseOrder",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseOrder_ApplicationUserId",
                table: "PurchaseOrder",
                column: "ApplicationUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_PurchaseOrder_AspNetUsers_ApplicationUserId",
                table: "PurchaseOrder",
                column: "ApplicationUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PurchaseOrder_AspNetUsers_ApplicationUserId",
                table: "PurchaseOrder");

            migrationBuilder.DropIndex(
                name: "IX_PurchaseOrder_ApplicationUserId",
                table: "PurchaseOrder");

            migrationBuilder.DropColumn(
                name: "ApplicationUserId",
                table: "PurchaseOrder");

            migrationBuilder.AddColumn<string>(
                name: "UserId",
                table: "PurchaseOrder",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseOrder_UserId",
                table: "PurchaseOrder",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_PurchaseOrder_AspNetUsers_UserId",
                table: "PurchaseOrder",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
