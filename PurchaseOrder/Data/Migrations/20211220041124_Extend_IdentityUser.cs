﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PurchaseOrder.Data.Migrations
{
    public partial class Extend_IdentityUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PurchaseOrder_AspNetUsers_IdentityUserId",
                table: "PurchaseOrder");

            migrationBuilder.DropIndex(
                name: "IX_PurchaseOrder_IdentityUserId",
                table: "PurchaseOrder");

            migrationBuilder.DropColumn(
                name: "IdentityUserId",
                table: "PurchaseOrder");

            migrationBuilder.AddColumn<string>(
                name: "UserId",
                table: "PurchaseOrder",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseOrder_UserId",
                table: "PurchaseOrder",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_PurchaseOrder_AspNetUsers_UserId",
                table: "PurchaseOrder",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PurchaseOrder_AspNetUsers_UserId",
                table: "PurchaseOrder");

            migrationBuilder.DropIndex(
                name: "IX_PurchaseOrder_UserId",
                table: "PurchaseOrder");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "PurchaseOrder");

            migrationBuilder.AddColumn<string>(
                name: "IdentityUserId",
                table: "PurchaseOrder",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseOrder_IdentityUserId",
                table: "PurchaseOrder",
                column: "IdentityUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_PurchaseOrder_AspNetUsers_IdentityUserId",
                table: "PurchaseOrder",
                column: "IdentityUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
