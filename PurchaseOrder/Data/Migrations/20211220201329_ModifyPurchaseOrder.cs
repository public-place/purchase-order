﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PurchaseOrder.Data.Migrations
{
    public partial class ModifyPurchaseOrder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LineItem_PurchaseOrder_PurchaseOrderID",
                table: "LineItem");

            migrationBuilder.AddForeignKey(
                name: "FK_LineItem_PurchaseOrder_PurchaseOrderID",
                table: "LineItem",
                column: "PurchaseOrderID",
                principalTable: "PurchaseOrder",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LineItem_PurchaseOrder_PurchaseOrderID",
                table: "LineItem");

            migrationBuilder.AddForeignKey(
                name: "FK_LineItem_PurchaseOrder_PurchaseOrderID",
                table: "LineItem",
                column: "PurchaseOrderID",
                principalTable: "PurchaseOrder",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
