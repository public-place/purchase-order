﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using PurchaseOrder.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PurchaseOrder.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<PurchaseOrder.Models.PurchaseOrder> PurchaseOrder { get; set; }
        public DbSet<LineItem> LineItem {get; set;}

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<PurchaseOrder.Models.PurchaseOrder>()
                .HasMany<LineItem>(order => order.LineItems)
                .WithOne(item => item.PurchaseOrder)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
