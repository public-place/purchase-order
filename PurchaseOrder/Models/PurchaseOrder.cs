﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using PurchaseOrder.Utils;

namespace PurchaseOrder.Models
{
    public class PurchaseOrder
    {
        [Key]
        public int ID { get; set; }
        
        [Required]
        public string Name { get; set; }
        
        /**
         * 'Status' can be 0 or non-zero.
         * 0: pending.
         * non-zero: submitted
         * */
        [Required]
        public int Status { get; set; }

        [DataType(DataType.DateTime), DisplayName("Created At")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime CreationDateTime { get; set; }

        // Navigation
        public string ApplicationUserId { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }

        [DisplayName("Total")]
        public double total
        {
            get
            {
                try
                {
                    return LineItems.Sum(i => i.Price);
                } catch(Exception e)
                {
                    return 0;
                }
            }
        }

        public List<LineItem> LineItems { get; set; }
        public PurchaseOrder()
        {
            // set timestamp of creation
            CreationDateTime = DateTime.Now;

            // auto-generated string for name
            Name = "Order-" + CreationDateTime.Year + "-" + PurchaseOrderUtil.GET_GENERATED_NAME();

            // set status as pending as default
            Status = 0;
        }
    }
}
