﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PurchaseOrder.Models
{
    public class LineItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }

        // Navigation
        public PurchaseOrder PurchaseOrder { get; set; }
    }
}
