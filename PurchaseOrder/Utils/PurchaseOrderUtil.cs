﻿using Microsoft.EntityFrameworkCore;
using PurchaseOrder.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PurchaseOrder.Utils
{
    public class PurchaseOrderUtil
    {
        private static Random random = new Random();
        public static string GET_GENERATED_NAME(int length = 10)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static bool VERIFY_IF_ORDER_IS_VALUABLE(int id, ApplicationDbContext context)
        {
            var purchaseOrder = context.PurchaseOrder
                .Include(order => order.LineItems)
                .FirstOrDefault(order => order.ID == id);

            if(purchaseOrder.LineItems.Count == 0)
            {
                context.Remove(purchaseOrder);
                context.SaveChanges();

                return false;
            }

            return true;

        }
    }
}
