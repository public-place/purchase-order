﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PurchaseOrder.Data;
using Microsoft.AspNetCore.Identity;
using PurchaseOrder.Models;
using Microsoft.EntityFrameworkCore;

namespace PurchaseOrder.Controllers
{
    public class PurchaseOrderController : BaseController
    {
        public PurchaseOrderController(ApplicationDbContext _context, UserManager<ApplicationUser> _userManager):
            base(_userManager, _context)
        {
            
        }
        public IActionResult Index()
        {
            getLoggedInUser();

            // get current user's orders
            var all = _context.PurchaseOrder
                .Include(order => order.LineItems)
                .Where(order => order.ApplicationUser == _currentUser)
                .ToList();

            int _todayOrderCount = todayOrderCount();

            ViewBag.todayOrderCount = _todayOrderCount;

            return View(all);
        }

        public IActionResult Create()
        {
            int _todayOrderCount = todayOrderCount();

            if (_todayOrderCount < 10)
            {
                return View();
            } else
            {
                return RedirectToAction(nameof(Index));
            }
        }


        public IActionResult Edit(int? id )
        {
            var order = _context.PurchaseOrder
                .Include(a => a.LineItems)
                .FirstOrDefault(x => x.ID == id);

            if (order == null)
            {
                return NotFound();
            }

            return View(order);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Submit(int id)
        {
            var order = _context.PurchaseOrder.Find(id);

            if (order == null)
            {
                return NotFound();
            }

            order.Status = 1;

            _context.PurchaseOrder.Update(order);
            _context.SaveChanges();

            return RedirectToAction(nameof(Index));
        }

        // GET: LineItems/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var purchaseOrder = await _context.PurchaseOrder
                .FirstOrDefaultAsync(m => m.ID == id);
            if (purchaseOrder == null)
            {
                return NotFound();
            }

            return View(purchaseOrder);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var purchaseOrder = await _context.PurchaseOrder
                .FirstOrDefaultAsync(m => m.ID == id);

            if (purchaseOrder == null)
            {
                return NotFound();
            }

            _context.PurchaseOrder.Remove(purchaseOrder);
            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }

        // Inner logic
        private int todayOrderCount()
        {
            int todayOrderCount = _context.PurchaseOrder
                .Where(order => order.CreationDateTime.Date == DateTime.Now.Date)
                .Count();
            return todayOrderCount;
        }
    }
}
