﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PurchaseOrder.Data;
using PurchaseOrder.Models;
using PurchaseOrder.Utils;

namespace PurchaseOrder.Controllers
{
    public class LineItemsController : BaseController
    {
        public LineItemsController(UserManager<ApplicationUser> userManager, ApplicationDbContext context):base(userManager, context)
        {}


        // GET: LineItems/Create
        public IActionResult Create(int? orderId, int? id)
        {
            return View();
        }

        // POST: LineItems/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PurchaseOrder,Id,Name,Price")] LineItem lineItem, int? orderId, int? id)
        {
            if (ModelState.IsValid)
            {
                this.getLoggedInUser();

                // if order is not exist then need to create order first
                if (orderId == null)
                {
                    var purchaseOrder = new PurchaseOrder.Models.PurchaseOrder();

                    purchaseOrder.ApplicationUser = this._currentUser;
                    _context.PurchaseOrder.Add(purchaseOrder);
                    _context.SaveChanges();

                    orderId = purchaseOrder.ID;
                }

                // retrive order
                var order = _context.PurchaseOrder.Find(orderId);

                lineItem.PurchaseOrder = order;
                _context.Add(lineItem);

                await _context.SaveChangesAsync();

                // redirec to order edit page since it has 1 item.
                return RedirectToRoute(new { controller = "PurchaseOrder", action = "Edit", id = orderId });
            }
            return View(lineItem);
        }

        // GET: LineItems/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var lineItem = await _context.LineItem
                .FirstOrDefaultAsync(m => m.Id == id);
            if (lineItem == null)
            {
                return NotFound();
            }

            return View(lineItem);
        }

        // POST: LineItems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var lineItem = _context.LineItem
                .Include(item=>item.PurchaseOrder)
                .FirstOrDefault(item=>item.Id == id);

            if(lineItem == null)
            {
                return NotFound();
            }

            _context.LineItem.Remove(lineItem);
            await _context.SaveChangesAsync();

            // check if order contains items or not.
            // if the order has no item then should be deleted
            bool valuable = PurchaseOrderUtil.VERIFY_IF_ORDER_IS_VALUABLE(lineItem.PurchaseOrder.ID, _context);

            if (valuable) // if contain any item.
            {
                return RedirectToRoute(new
                {
                    controller = "PurchaseOrder",
                    action = "Edit",
                    id = lineItem.PurchaseOrder.ID
                });
            } else // if order is deleted then redirect to index page. 
            {
                return RedirectToRoute(new
                {
                    controller = "PurchaseOrder",
                    action = "Index"
                });
            }
        }

        private bool LineItemExists(int id)
        {
            return _context.LineItem.Any(e => e.Id == id);
        }
    }
}
