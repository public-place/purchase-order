﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PurchaseOrder.Data;
using PurchaseOrder.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PurchaseOrder.Controllers
{
    public class BaseController : Controller
    {
        protected ApplicationUser _currentUser;

        protected readonly UserManager<ApplicationUser> _userManager;
        protected readonly ApplicationDbContext _context;

        public BaseController(UserManager<ApplicationUser> _userManager, ApplicationDbContext _context)
        {
            this._userManager = _userManager;
            this._context = _context;
        }

        /**
         * Get current logged in user
         */
        protected void getLoggedInUser()
        {
            this._currentUser = this._userManager.FindByIdAsync(_userManager.GetUserId(HttpContext.User)).Result;
        }
    }
}
